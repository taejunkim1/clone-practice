import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class masking{

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] userInput = new String[3];
        System.out.print("name: ");
        userInput[0] = br.readLine();
        System.out.print("username: ");
        userInput[1] = br.readLine();
        System.out.print("email: ");
        userInput[2] = br.readLine();

        for(int i=0; i< userInput.length; i++){
            int inputLength;
            if(i == 2){
                inputLength = userInput[i].split("@")[0].length();
            }
            else{
                inputLength = userInput[i].length();
            }

            if(inputLength >=5){
                StringBuffer buff = new StringBuffer(userInput[i]);
                userInput[i] = buff.replace(2, inputLength-1, "*".repeat(inputLength-3)).toString();
            }
            else{
                StringBuffer buff = new StringBuffer(userInput[i]);
                buff.replace(0,1, "*");
                buff.replace(inputLength-1,inputLength, "*");
                userInput[i] = buff.toString();
            }
        }

        System.out.println("name: " + userInput[0]);
        System.out.println("username: " + userInput[1]);
        System.out.println("email: " + userInput[2]);
    }
}